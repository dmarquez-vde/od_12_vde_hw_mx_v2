# -*- coding: utf-8 -*-

from odoo import api, fields, models, _

class hwExecution(models.Model):
    _name = "hw.execution"

    _inherit = ['mail.thread', 'mail.activity.mixin']

    typeDoc = fields.Selection([(1,'Ingreso'),(2,'Egreso')], string="Document Type")
    dateStart = fields.Datetime(string="Start Date")
    dateEnd = fields.Datetime(string="End Date")
    dateCron = fields.Datetime(string="Cron Date")
    dateExecution = fields.Datetime(string="Execution Date")
    #status = fields.Selection([(0,'Ok'),(1,'Error')], default=1, string="Status", index=True, track_visibility='onchange',copy=False)
    #state = fields.Selection(selection=[('pending', 'En Proceso'),('success','Timbrado'),('error','Error')], default='pending', string="Status", index=True, track_visibility='onchange',copy=False)
    state = fields.Selection(selection=[(0,'Ok'),(1,'Error')], default=1, string="Status", index=True, track_visibility='onchange',copy=False)
    params_crypt = fields.Text(string="Cryp Params")
    params = fields.Text(string="Params")
    token = fields.Text(string="Token")
#    cfdi_ids = fields.One2many('hw.cfdi','execution_id',string="CFDIs")
    cfdi_ids = fields.One2many('account.invoice','execution_id',string="CFDIs")
    res_ids = fields.One2many('hw.result','execution_id',string="Results")
