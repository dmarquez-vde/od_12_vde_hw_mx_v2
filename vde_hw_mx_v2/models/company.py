# -*- coding: utf-8 -*-

from odoo import api, fields, models, _

class ResCompany(models.Model):
    _inherit = "res.company"

    @api.multi
    def _get_domicilio(self):
        for record in self:
            domicilio = record.street + ", " + record.street2 + ", " + record.city + ", " + record.state_id.name + ", " + record.zip + ", "+record.country_id.name
            record.vde_domicilio = domicilio

    sat_regimen = fields.Selection([
        ('601','General de Ley Personas Morales'),
        ('603','Personas Morales con Fines no Lucrativos'),
        ('605','Sueldos y Salarios e Ingresos Asimilados a Salarios'),
        ('606','Arrendamiento'),
        ('608','Demás ingresos'),
        ('609','Consolidación'),
        ('610','Residentes en el Extranjero sin Establecimiento Permanente en México'),
        ('611','Ingresos por Dividendos (socios y accionistas)'),
        ('612','Personas Físicas con Actividades Empresariales y Profesionales'),
        ('614','Ingresos por intereses'),
        ('616','Sin obligaciones fiscales'),
        ('620','Sociedades Cooperativas de Producción que optan por diferir sus ingresos'),
        ('621','Incorporación Fiscal'),
        ('622','Actividades Agrícolas, Ganaderas, Silvícolas y Pesqueras'),
        ('623','Opcional para Grupos de Sociedades'),
        ('624','Coordinados'),
        ('628','Hidrocarburos'),
        ('607','Régimen de Enajenación o Adquisición de Bienes'),
        ('629','De los Regímenes Fiscales Preferentes y de las Empresas Multinacionales'),
        ('630','Enajenación de acciones en bolsa de valores'),
        ('615','Régimen de los ingresos por obtención de premios')
        ], string="Régimen Fiscal")
    sat_cer = fields.Binary(string="Cer File")
    sat_key = fields.Binary(string="key File")
    sat_num_cer = fields.Char(string="Cer Num")
    sat_certificado_pass = fields.Char(string="Cer Pass")
    vde_dir_xml = fields.Char(string="Path XML")
    vde_url_logo = fields.Char(string="Path logo")
    vde_domicilio = fields.Char(string="Adress", compute="_get_domicilio", store=True)
    stamp_no = fields.Integer(string="Stamp CFDI Allowed", default=100)
