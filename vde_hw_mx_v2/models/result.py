# -*- coding: utf-8 -*-

from odoo import api, fields, models, _


class hwResult(models.Model):
    _name = "hw.result"

    resultCode = fields.Char(string="Result Code")
    resultData = fields.Text(string="Result Data")
    nextPage = fields.Char(string="Result Next Page")
    message = fields.Char(string="Result Message")
    execution_id = fields.Many2one('hw.execution', string="Execution")
    guid = fields.Char(string="GUID")
    json = fields.Text(string="JSON")