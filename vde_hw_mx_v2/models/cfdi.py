# -*- coding: utf-8 -*-

from odoo import api, fields, models, _
import base64
import time
import logging


class hwCFDI(models.Model):
    _inherit = "account.invoice"

#    _inherit = ['mail.thread', 'mail.activity.mixin']

    """
    serie = fields.Char(string="Serie")
    folio = fields.Char(string="Folio")
    name = fields.Char(string="Factura")
    state = fields.Selection(selection=[('pending', 'En Proceso'),('success','Timbrado'),('error','Error')], default='pending', string="Status", index=True, track_visibility='onchange',copy=False)
    pdf = fields.Binary(string="PDF")
    xml = fields.Binary(string="XML")
    xml_string = fields.Text(string="XML string")
    cbb = fields.Binary(string="CBB")
    cbb_string = fields.Text(string="CBB string")
    html = fields.Binary(string="HTML")
    html_string = fields.Text(string="HTML string")
    uuid = fields.Char(string="UUID")
    date_stamp = fields.Datetime(string="Date Stamping")
    hw_no_invoice = fields.Char(string="No Invoice HW")
    hw_track_id = fields.Char(string="Track ID HW")
    hw_json = fields.Text(string="HW json")
    vde_cfdi_assoc = fields.Text(string="VDE CFDI array")
    date_entry = fields.Datetime(string="Date Entry")
    result = fields.Text(string="Result")
    date_push = fields.Datetime(string="Date Push")
    put_result_code = fields.Char(string="Put Result Code")
    put_date = fields.Datetime(string="Date Put")
    put_message = fields.Char(string="Put Message")
    is_send = fields.Boolean(string="Is Email send?")
    send_mail_account = fields.Char(string="Email account")
    send_mail_name = fields.Char(string="Email Name")
    #date_creation = fields.Datetime(string="Date Creation")
    url_pdf = fields.Char(string="URL PDF")
    url_xml = fields.Char(string="URL XML")
    sync = fields.Integer(string="Sync")
    execution_id = fields.Many2one('hw.execution', string="Execution")
    typeDoc = fields.Selection(related='execution_id.typeDoc', string="Document Type")
    """

    hw_no_invoice = fields.Char('No Invoice HW')
    hw_track_id = fields.Char('Track ID HW')
    hw_date_entry = fields.Datetime('Date Entry')
    hw_put_result_code = fields.Char('Put Result Code')
    hw_put_message = fields.Char('Put Message')
    hw_put_date = fields.Datetime('Put Date')
    hw_json = fields.Text('JSON HW')
    hw_cfdi_array = fields.Text('CFDI')
    hw_xml_url = fields.Char('URL XML')
    execution_id = fields.Many2one('cfdi.execution', string="Execution")
    state_vde = fields.Selection(selection=[('pending', 'En Proceso'),('success','Timbrado'),('error','Error')], default='pending', string="Status", index=True, track_visibility='onchange',copy=False)
    result = fields.Text(string="Result")
    date_push = fields.Datetime(string="Date Push")
    put_result_code = fields.Char(string="Put Result Code")
    put_date = fields.Datetime(string="Date Put")
    put_message = fields.Char(string="Put Message")
    send_mail_account = fields.Char(string="Email account")
    send_mail_name = fields.Char(string="Email Name")
    completed = fields.Boolean(string="Completed?",default=False)

    #Campos Log Moficiaciones
    update_support = fields.One2many('vde.invoice.update', 'invoice_id', string="Log Actividad", store=True)

    @api.one
    def compute_amount(self):
        self._compute_amount()
        return True

    @api.multi
    def get_pdf(self,report):
#        pdf = self.env.ref('vde_report_invoice_zeis.invoice_zeis').sudo().render_qweb_pdf([self.id])[0]
        pdf = self.env.ref(report).sudo().render_qweb_pdf([self.id])[0]
        pdf = base64.b64encode(pdf)
        return pdf
    """
    @api.multi
    def compute_taxes(self):
        print("dm<<<<<<a")
        account_invoice_tax = self.env['account.invoice.tax']
        ctx = dict(self._context)
        for invoice in self:
            # Delete non-manual tax lines
            self._cr.execute("DELETE FROM account_invoice_tax WHERE invoice_id=%s AND manual is False", (invoice.id,))
            if self._cr.rowcount:
                self.invalidate_cache()

            # Generate one tax line per tax, however many invoice lines it's applied to
            tax_grouped = invoice.get_taxes_values()

            # Create new tax lines
            for tax in tax_grouped.values():
                tax.update({'amount':round(tax.get('amount'),2)})
                print("tax ", tax)
                account_invoice_tax.create(tax)

        # dummy write on self to trigger recomputations
        return self.with_context(ctx).write({'invoice_line_ids': []})
    """

    @api.multi
    def get_taxes_values(self):
        tax_grouped = {}
        round_curr = self.currency_id.round
        for line in self.invoice_line_ids:
            if not line.account_id or line.display_type:
                continue
            #price_unit = line.price_unit * (1 - (line.discount or 0.0) / 100.0)
            price_unit = line.price_unit * (1 - (line.discount or 0.0) / 100.0) if line.discount > 0 else line.price_unit - (line.discount_line/line.quantity)
            taxes = line.invoice_line_tax_ids.compute_all(price_unit, self.currency_id, line.quantity, line.product_id, self.partner_id)['taxes']
            for tax in taxes:
                val = self._prepare_tax_line_vals(line, tax)
                key = self.env['account.tax'].browse(tax['id']).get_grouping_key(val)

                if key not in tax_grouped:
                    tax_grouped[key] = val
                    tax_grouped[key]['base'] = round_curr(val['base'])
                else:
                    tax_grouped[key]['amount'] += val['amount']
                    tax_grouped[key]['base'] += round_curr(val['base'])
        return tax_grouped

    @api.one
    @api.depends('invoice_line_ids.price_subtotal', 'tax_line_ids.amount', 'currency_id', 'company_id', 'date_invoice', 'type')
    def _compute_amount(self):
        round_curr = self.currency_id.round
        amount_untaxed = sum(line.price_subtotal_sn for line in self.invoice_line_ids)
        self.amount_untaxed = amount_untaxed
        #self.total_antes_descuento = sum((line.price_subtotal_sn) for line in self.invoice_line_ids)
        #self.descuento_total = sum(round(((line.price_unit*line.quantity) * (line.discount/100)),2) for line in self.invoice_line_ids)
        total_antes_descuento = sum((line.price_subtotal_sn) for line in self.invoice_line_ids)
        self.total_antes_descuento = total_antes_descuento
        self.descuento_total = sum(line.discount_line for line in self.invoice_line_ids)
        #self.descuento_total = total_antes_descuento - amount_untaxed
        #self.amount_untaxed = total_antes_descuento - sum(line.discount_line for line in self.invoice_line_ids)

        self.amount_tax = sum(round_curr(line.amount) for line in self.tax_line_ids)
        #self.amount_tax = 0

        #self.amount_tax = sum(line.line_tax1 for line in self.invoice_line_ids)
        self.amount_tax1 = sum(line.line_tax1 for line in self.invoice_line_ids)
        self.amount_tax2 = sum(line.line_tax2 for line in self.invoice_line_ids)

        self.amount_total = abs(self.amount_untaxed + self.amount_tax - self.descuento_total)
        amount_total_company_signed = self.amount_total
        amount_untaxed_signed = self.amount_untaxed
        if self.currency_id and self.company_id and self.currency_id != self.company_id.currency_id:
            currency_id = self.currency_id.with_context(date=self.date_invoice)
            amount_total_company_signed = currency_id.compute(self.amount_total, self.company_id.currency_id)
            amount_untaxed_signed = currency_id.compute(self.amount_untaxed, self.company_id.currency_id)
        sign = self.type in ['in_refund', 'out_refund'] and -1 or 1
        self.amount_total_company_signed = amount_total_company_signed * sign
        self.amount_total_signed = self.amount_total * sign
        self.amount_untaxed_signed = amount_untaxed_signed * sign
        self.amount_untaxed = amount_untaxed - self.descuento_total

class AccountInvoiceLine(models.Model):
    _inherit = 'account.invoice.line'
        
    clave_unidad = fields.Many2one('c.claveunidad', string="Unidad Medida SAT")
    clave_producto = fields.Char(string="Clave Producto")
    discount_line = fields.Monetary(string='Descuento por linea')

    @api.one
    @api.depends('price_unit', 'discount', 'quantity','product_id', 'invoice_id.partner_id', 'invoice_id.currency_id')

    def _compute_price_tax(self):
            _logger = logging.getLogger(__name__)
            _logger.info("Mensaje Informativo o Print")
            #_logger.info(ltax )

            for ltax_ in self.invoice_line_tax_ids:
                    ltax = (ltax_.amount or 0.0) /100
                    _logger.info(ltax_.amount)
                    if(ltax_.amount==16.0):
                            self.line_tax1 = self.price_subtotal * ltax
                            _logger.info("si entra en 16")
                    else:
                            _logger.info(str(ltax_.name));
                            if(str(ltax_.amount)=='-4.0'):
                                    self.line_tax2 = self.price_subtotal * ltax
                                    _logger.info("entra a 4")


                    #ltax = ((self.invoice_line_tax_ids.amount or 0.0) / 100)
                    #self.price_with_tax = self.price_subtotal * ltax
            #self.discount_line =  (self.price_unit  * self.quantity) * (self.discount/100)
            self.price_subtotal_sn =  (self.price_unit  * self.quantity) - self.discount

            #_logger = logging.getLogger(__name__)
            #_logger.info("Mensaje Informativo o Print")
            #_logger.info(self.invoice_line_tax_ids.name )
            #_logger.info(ltax )

            if self.invoice_id:
                    self.line_tax1 = self.line_tax1
                    #self.discount_line = self.invoice_id.currency_id.round(self.discount_line)
                    self.line_tax2 = self.invoice_id.currency_id.round(self.line_tax2)
                    self.price_subtotal_sn = self.invoice_id.currency_id.round(self.price_subtotal_sn)
                    #self.price_subtotal = self.price_subtotal_sn
                    self.price_subtotal_signed = self.price_subtotal_sn

    @api.one
    @api.depends('price_unit', 'discount', 'invoice_line_tax_ids', 'quantity','product_id', 'invoice_id.partner_id', 'invoice_id.currency_id', 'invoice_id.company_id','invoice_id.date_invoice')
    def _compute_price(self):
            currency = self.invoice_id and self.invoice_id.currency_id or None
            price = self.price_unit * (1 - (self.discount or 0.0) / 100.0)
            taxes = False
            if self.invoice_line_tax_ids:
                taxes = self.invoice_line_tax_ids.compute_all(price, currency, self.quantity, product=self.product_id, partner=self.invoice_id.partner_id)
            self.price_subtotal = price_subtotal_signed = (self.quantity * price) - self.discount_line
            if self.invoice_id.currency_id and self.invoice_id.company_id and self.invoice_id.currency_id != self.invoice_id.company_id.currency_id:
                price_subtotal_signed = self.invoice_id.currency_id.with_context(date=self.invoice_id.date_invoice).compute(price_subtotal_signed, self.invoice_id.company_id.currency_id)
            sign = self.invoice_id.type in ['in_refund', 'out_refund'] and -1 or 1
            self.price_subtotal_signed = price_subtotal_signed * sign
"""
    @api.one
    @api.depends('price_unit', 'discount', 'quantity','product_id', 'invoice_id.partner_id', 'invoice_id.currency_id')
    def _compute_price_tax(self):
            _logger = logging.getLogger(__name__)
            _logger.info("Mensaje Informativo o Print")
            #_logger.info(ltax )

            for ltax_ in self.invoice_line_tax_ids:
                    ltax = (ltax_.amount or 0.0) /100
                    _logger.info(ltax_.amount)
                    if(ltax_.amount==16.0):
                            self.line_tax1 = round(self.price_subtotal * ltax,2)
                            _logger.info("si entra en 16")
                    else:
                            _logger.info(str(ltax_.name));
                            if(str(ltax_.amount)=='-4.0'):
                                    self.line_tax2 = round(self.price_subtotal * ltax,2)
                                    _logger.info("entra a 4")


                    #ltax = ((self.invoice_line_tax_ids.amount or 0.0) / 100)
                    #self.price_with_tax = self.price_subtotal * ltax
            self.discount_line =  (self.price_unit  * self.quantity) * (self.discount/100)
            self.price_subtotal_sn =  (self.price_unit  * self.quantity)

            #_logger = logging.getLogger(__name__)
            #_logger.info("Mensaje Informativo o Print")
            #_logger.info(self.invoice_line_tax_ids.name )
            #_logger.info(ltax )

            if self.invoice_id:
                    self.line_tax1 = self.line_tax1
                    self.discount_line = self.invoice_id.currency_id.round(self.discount_line)
                    self.line_tax2 = self.invoice_id.currency_id.round(self.line_tax2)
                    self.price_subtotal_sn = self.invoice_id.currency_id.round(self.price_subtotal_sn)
                    #self.price_subtotal = self.price_subtotal_sn
                    self.price_subtotal_signed = self.price_subtotal_sn
"""

class VdeInvoiceUpdate(models.Model):

    _name = 'vde.invoice.update'

    invoice_id = fields.Many2one('account.invoice', string="CFDI", readonly=True)
    name = fields.Selection(selection=[('factura_creada', 'CFDI Creado'),
     ('factura_actualizada', 'CFDI Actualizado'),
     ('factura_error', 'CFDI con Error'),
     ('factura_timbrada', 'CFDI Timbrado')], string=_('Accion'), default='factura_creada', readonly=True, copy=False)
    description = fields.Text(string="Descripción",readonly=True)


class ResPartner(models.Model):
    _inherit = 'res.partner'

    @api.constrains('vat', 'country_id')
    def check_vat(self):
        # check_vat is implemented by base_vat which this localization
        # doesn't directly depend on. It is however automatically
        # installed for Colombia.
        if self.sudo().env.ref('base.module_base_vat').state == 'installed':
            # don't check Colombian partners unless they have RUT (= Colombian VAT) set as document type
#            self = self.filtered(lambda partner: partner.country_id != self.env.ref('base.co') or\
#                                                 partner.l10n_co_document_type == 'rut')
#            return super(ResPartner, self).check_vat()
            return True
        else:
            return True
